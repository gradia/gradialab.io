+++
title = "Illyria"
categories = ["Geography"]
date = "Mon Jan 16 13:47:35 2012"
+++

[<img src="/files/illyria/map-illyria.png"  align="right"
width="400">](/files/illyria/map-illyria.png)

The Duchy of Illyria, to the northeast of central [Narin]({{< relref
"empire-of-narin.md" >}}) and on the other side of the [Anandies Mountains]({{<
relref "anandies-mountains.md" >}}), was several times a Duchy within the
Empire of Narin.

In the south of Illyria, nestled within the base of the Anandies Mountains, is
the stronghold of [Nyrdall]({{< relref "nyrdall.md" >}}), which was captured by
Narin and used as a central point in their last campaign to conquer Illyria.

#  Characters of Note 

* **[Era: Empire in Decline]({{< relref "era-empire-in-decline.md" >}})**
 * [The Morcuru]({{< relref "the-morcuru.md" >}})
 * [Kor]({{< relref "kor.md" >}})
 * [Jayden]({{< relref "jayden.md" >}})
* **[Era: The Sun Schism]({{< relref "era-the-sun-schism.md" >}})**
 * [The Sun Prince]({{< relref "the-sun-prince.md" >}})
 * [Revenna]({{< relref "revenna.md" >}})
 * [Valore]({{< relref "valore.md" >}})
 * [Kaol]({{< relref "kaol.md" >}})
 
TODO

#  Cities of Note 

[Illyria City]({{< relref "illyria-city.md" >}}) is the capital of Illyria.
[Dol Kor]({{< relref "dol-kor.md" >}}) is a small city at the far eastern end
of Illyrian control, which is often viewed as a symbol of good relations
between Illyria and the currently-rebuilding city of [Eindonallan]({{< relref
"eindonallan.md" >}}).
