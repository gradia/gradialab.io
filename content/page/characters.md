+++
title = "Characters"
categories = []
date = "Tue Nov 25 12:22:11 2008"
+++

What of those people whose names echo down through time? History, legend and
record differ, but the true stories of these heroes and villains are now lost.
In Gradia, the myth becomes the truth.

* [Morcuru]({{< relref "morcuru.md" >}}), Prophet of Flame
* [Kor]({{< relref "kor.md" >}}), Mongrel Champion
* [Jayden]({{< relref "jayden.md" >}}), Lady of the Sun
* [Dominic]({{< relref "dominic.md" >}}), Oracle for Hire
* [Mandrellan]({{< relref "mandrellan.md" >}}), the Greatest Swordsman to Ever Live
* [Argali]({{< relref "argali.md" >}}) of the Four Winds
* [Doltain]({{< relref "doltain.md" >}}), Bear Man of Eindonallan
* [Volker]({{< relref "volker.md" >}}), Walker of the Wierdlands
* [Kojin]({{< relref "kojin.md" >}}), Fallen Knight of Xar
* [Erzuli]({{< relref "erzuli.md" >}}), Urchin Extraordinaire
* [Ravenna]({{< relref "ravenna.md" >}}), Arcane Countess
* [Tashtash]({{< relref "tashtash.md" >}}) of Many Shapes
* [Caligari]({{< relref "caligari.md" >}}), Rogue Alchemist
